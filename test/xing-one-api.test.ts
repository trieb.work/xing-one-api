import init from "../src/init"
import { getFullProfile } from "../src/profile/index"
import dummy from '../src/xing-one-api'

/**
 * Create this file on your own and add your cookie string for testing
 */
import values from './test_values'

const cookieString = values.cookieString
const csrftoken = 'VVVVVVV'
const userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"

/**
 * This test is trying to get the full profile from XING-One in order to check authentication etc..
 */
describe("Test", () => {
  it("tests if we can pull a profile", async () => {
    init( {  cookieString, csrftoken, userAgent })
    const { ZohoCRMprofile } = await getFullProfile({ id: "Michael_Flunger"})
    console.log('Got this CRM profile:', ZohoCRMprofile) 
    expect(ZohoCRMprofile).toBeDefined
   
   
  })
  it("does nothing but calling the default function", async ()=> {
    let result = dummy()
    expect(result).toBeDefined
  })

})


import { ApolloClient } from 'apollo-boost'
import { InMemoryCache } from 'apollo-boost'
import fetch from 'isomorphic-fetch'
import { HttpLink } from 'apollo-boost'

import { GlobalUserAgent, GlobalSessionCookies } from '../globals'
/*  
*   This helper methods creates the apollo instance with appropriate headers
*/
export const createInstance = () => {

    if (!GlobalUserAgent) throw new Error('No user-agent found. Please set one via the init function')
    if (!GlobalSessionCookies) throw new Error('No session cookies found. Please set them via the init function')
    const link = new HttpLink({
        uri: "https://www.xing.com/xing-one/api",
        // uri: "https://linki.free.beeceptor.com/xing-one",
        fetch,
        headers: {
            "xing-one-preview": "true",
            "cookie": GlobalSessionCookies,
            "User-Agent" : GlobalUserAgent,
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
            "pragma": "no-cache",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin"
        }
        
    })
    
    const client = new ApolloClient({
        link: link,
        cache: new InMemoryCache()
    });

    return client
 



}

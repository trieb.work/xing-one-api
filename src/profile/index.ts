import { createInstance } from '../utils/createInstance'
import { profileModulesQuery, timelineQuery } from '../queries/xinguser.gql'


/**
* 
* get the full user profile using the profile-id as input parameter
*
*/ 
export async function getFullProfile({ id }: { id :string }){
    const client = createInstance()


    let profile = {
        Last_Name: '',
        First_Name:'',
        Email:'',
        Mobile: '',
        Phone: '',
        'XING_ID': id,
        Company:'',
        Designation: '',
        City: '',
        Zip_Code: '',
        Street: '',
        Country: '',
        CountryCode: ''
    }
    let data = await client.query({
        query: profileModulesQuery,
        fetchPolicy: 'no-cache',
        variables: {
            "profileId": id,
            "actionsFilter": ["ADD_CONTACT", "ADVERTISE_PROFILE", "BLOCK_USER", "BOOKMARK_USER", "CONFIRM_CONTACT", "EDIT_XING_ID"]
        }
    })
    if (!data.data || !data.data.profileModules) throw new Error('No valid response from XING - are your cookies valid? '+JSON.stringify(data, null,2))

    profile.Last_Name = data.data['profileModules'].xingIdModule.xingId.lastName
    profile.First_Name = data.data['profileModules'].xingIdModule.xingId.firstName
    let contactdetails: any = {...data.data['profileModules'].xingIdModule.contactDetails}
    if (contactdetails.business){
        if (contactdetails.business.address){
            profile['CountryCode'] = contactdetails.business.address.country.countryCode
            profile['Country'] = contactdetails.business.address.country.name
            profile['City'] =  contactdetails.business.address.city 
            profile['Zip_Code'] = contactdetails.business.address.zip || ""
            profile['Street'] = contactdetails.business.address.street || ""
        }
          
        if(contactdetails.business.email) profile.Email = contactdetails.business.email
        if(contactdetails.business.phone) profile.Phone = contactdetails.business.phone.phoneNumber
        if(contactdetails.business.mobile) profile.Mobile = contactdetails.business.mobile.phoneNumber


    }

    const data2 = await client.query({
        query: timelineQuery,
        fetchPolicy: 'no-cache',
        variables: {
                "id": id
            }
        
    })

    let Tempdata = data2.data.profileModules.timelineModule.buckets[0]
    // check if the first entrie is the current profession.
    if(Tempdata['entries'] && Tempdata['entries'].length > 0 && Tempdata['entries'][0].isCurrent){
        profile.Designation = Tempdata['entries'][0].title
        profile.Company = Tempdata['entries'][0].organization.name
    }


    return { ZohoCRMprofile: profile }


}
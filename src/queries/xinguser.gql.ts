import { gql } from 'apollo-boost';

export const profileModulesQuery = gql`
    query getXingId($profileId: SlugOrID!, $actionsFilter: [AvailableAction!]) {
        profileModules(id: $profileId) {
            __typename
            xingIdModule(actionsFilter: $actionsFilter) {
                xingId {
                    id
                    firstName
                    lastName
                }
                ...xingIdContactDetails
                ...xingIdModuleCta
            }
        }
    }
    fragment xingIdContactDetails on XingIdModule {
        contactDetails {
            business {
            address {
                city
                country {
                countryCode
                name: localizationValue
                }
                province {
                id
                canonicalName
                name: localizationValue
                }
                street
                zip
                __typename
            }
            email
            fax {
                phoneNumber
            }
            mobile {
                phoneNumber
            }
            phone {
                phoneNumber
            }
            __typename
            }
            private {
            address {
                city
                country {
                countryCode
                name: localizationValue
                __typename
                }
                province {
                id
                canonicalName
                name: localizationValue
                __typename
                }
                street
                zip
                __typename
            }
            email
            fax {
                phoneNumber
                __typename
            }
            mobile {
                phoneNumber
                __typename
            }
            phone {
                phoneNumber
                __typename
            }
            __typename
            }
            __typename
        }
    __typename
    }

    fragment xingIdModuleCta on XingIdModule {
    actions {
        __typename
        label
    }
    __typename
    } ` 

export const timelineQuery = gql`
    query getProfileTimeline($id: SlugOrID!) {
    viewer {
        featureSwitches
        __typename
    }
    profileModules(id: $id) {
        timelineModule {
        ...ProfileTimelineModuleInnerData
        title
        __typename
        }
        __typename
    }
    }

    fragment ProfileTimelineModuleInnerData on ProfileTimelineModule {
    buckets {
        entries {
        additionalData {
            projobsData {
            ... on ProJobsWorkExperience {
                formattedBudgetAmount
                formattedResponsibility
                formattedResponsibilityTeamSize
                formattedRevenueAmount
                resultsAndAchievements
                tasksAndActions
                topicsAndProjects
                __typename
            }
            ... on ProJobsEducation {
                educationDetails
                grades
                __typename
            }
            __typename
            }
            __typename
        }
        degree
        description
        isCurrent
        organization {
            __typename
            ... on ProfileEducationalInstitution {
            name
            __typename
            }
            ... on ProfileCompany {
            name
            company {
                address {
                city
                country {
                    localizationValue
                    __typename
                }
                __typename
                }
                companyName
                companySize
                industry {
                localizationValue
                __typename
                }
                links {
                public
                __typename
                }
                logos {
                logo256px
                __typename
                }
                __typename
            }
            industry {
                localizationValue
                __typename
            }
            __typename
            }
        }
        occupationType {
            localizationValue
            __typename
        }
        location {
            city
            __typename
        }
        localizedTimeString
        title
        website {
            url
            __typename
        }
        __typename
        }
        localizationValue
        __typename
    }
    globalId
    __typename
    }`
    
export const personalDetails = gql`
    query personalDetails($id: SlugOrID!) {
    profileModules(id: $id) {
        personalDetailsModule {
        title
        personalDetails {
            globalId
            birthDate {
            day
            month
            year
            __typename
            }
            birthName
            __typename
        }
        __typename
        }
        __typename
    }
    }`
